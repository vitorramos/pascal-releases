# Install

```c++
sudo sh -c "$(curl -fsSL https://gitlab.com/ramos.vitor89/pascal-releases/-/raw/main/install.sh)"
```

# Install locally

```c++
sh -c "$(curl -fsSL https://gitlab.com/ramos.vitor89/pascal-releases/-/raw/main/install_local.sh);"
source pascal/env.sh
```