#!/bin/sh

curl https://gitlab.com/ramos.vitor89/pascal-releases/-/raw/main/pascalanalyzer --output pascalanalyzer
curl https://gitlab.com/ramos.vitor89/pascal-releases/-/raw/main/libmanualinst.so --output libmanualinst.so
curl https://gitlab.com/ramos.vitor89/pascal-releases/-/raw/main/libautoinst.so --output libautoinst.so
curl https://gitlab.com/ramos.vitor89/pascal-releases/-/raw/main/env.sh --output env.sh

chmod +x pascalanalyzer

mkdir -p pascal
mkdir -p pascal/libs
mkdir -p pascal/bin

mv libautoinst.so libmanualinst.so pascal/libs
mv pascalanalyzer pascal/bin
mv env.sh pascal/